import random
def process_textfile(filename):
    d={}
    q=open(filename, 'r')
    f=q.read()
    f=f.replace('/n',' ')
    f1=f.split()
    d.update({'BEGIN':set([f1[0]])})
    d.update({f1[-1]:set(['END'])})
    for i in range(len(f1)-1):
        if f1[i][-1]=='.' or f1[i][-1]=='?' or f1[i][-1]=='!':
            if f1[i] in d:
                d.update({f1[i]:d[f1[i]]|set(['END'])})
                d.update({'BEGIN':d['BEGIN']|set([f1[i+1]])})
            else:
                d.update({f1[i]:set(['END'])})
                d.update({'BEGIN':d['BEGIN']|set([f1[i+1]])})
        else:
            if f1[i]in d:
                d.update({f1[i]:d[f1[i]]|set([f1[i+1]])})
            else:
                d.update({f1[i]:set([f1[i+1]])})
    return d

def generate_line(d):
    curr='BEGIN'
    res=[]
    while curr!='END':
        res+=[curr]
        curr=list(d[curr])[random.randint(0,len(d[curr])-1)]
    return(' '.join(res[1:]))

print(generate_line(process_textfile('text.txt')))
